package com.usarb.restclient.service;

import com.usarb.restclient.model.Department;
import com.usarb.restclient.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService extends TenantService<Department> {

    public DepartmentService(DepartmentRepository repository) {
        super(repository);
    }
}
