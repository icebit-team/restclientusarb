package com.usarb.restclient.service;

import com.usarb.restclient.model.Faculty;
import com.usarb.restclient.repository.FacultyRepository;
import org.springframework.stereotype.Service;

@Service
public class FacultyService extends TenantService<Faculty> {

    public FacultyService(FacultyRepository repository) {
        super(repository);
    }
}
