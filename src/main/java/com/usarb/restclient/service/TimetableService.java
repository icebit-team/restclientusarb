package com.usarb.restclient.service;

import com.usarb.restclient.model.*;
import com.usarb.restclient.repository.TimetableRepository;
import com.usarb.restclient.utils.ApplicationException;
import com.usarb.restclient.utils.Constants;
import com.usarb.restclient.utils.TimetableFilter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Subgraph;
import javax.persistence.criteria.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class TimetableService extends TenantService<Timetable> {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private SemesterService semesterService;

    public TimetableService(TimetableRepository repository) {
        super(repository);
    }

    public List<Timetable> findCriteriaAll(String tenant, Long id, String groupName, String teacherName, String classroomName, Long groupId, Long teacherId, Long classroomId,
                                           Integer weekNumber, Integer dayNumber, Integer semesterNumber, LocalDate weekDate) {
        return findCriteriaAll(TimetableFilter.buildDefault(tenant, id, groupName, teacherName, classroomName, groupId, teacherId, classroomId, weekNumber, dayNumber, semesterNumber, weekDate));
    }

    public List<Timetable> findCriteriaAll(TimetableFilter filter) {
        if (filter.getWeekNumber() != null && filter.getWeekDate() != null) {
            throw new ApplicationException("Conflict of parameters. Use 'weekNumber and semesterNumber' or 'weekDate'");
        }

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Timetable> query = criteriaBuilder.createQuery(Timetable.class);

        Root<Timetable> root = query.from(Timetable.class);

        Join<Timetable, Lesson> lessonJoin = root.join(Timetable_.lesson, JoinType.LEFT);
        Join<Lesson, Torrent> torrentJoin = lessonJoin.join(Lesson_.torrent, JoinType.LEFT);
        Join<Torrent, Teacher> teacherJoin = torrentJoin.join(Torrent_.teacher, JoinType.LEFT);
        Join<Torrent, Semester> semesterJoin = torrentJoin.join(Torrent_.semester, JoinType.LEFT);
        Join<Lesson, Group> groupJoin = lessonJoin.join(Lesson_.group, JoinType.LEFT);
        Join<Timetable, Classroom> classroomJoin = root.join(Timetable_.classroom, JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();

        if (filter.getId() != null) {
            predicates.add(criteriaBuilder.equal(root.get(Timetable_.id), filter.getId()));
        }

        if (filter.getGroupName() != null) {
            predicates.add(criteriaBuilder.equal(groupJoin.get(Group_.name), filter.getGroupName()));
        }

        if (filter.getTeacherName() != null) {
            predicates.add(criteriaBuilder.equal(teacherJoin.get(Teacher_.name), filter.getTeacherName()));
        }

        if (filter.getClassroomName() != null) {
            predicates.add(criteriaBuilder.equal(classroomJoin.get(Classroom_.name), filter.getClassroomName()));
        }

        if (filter.getGroupId() != null) {
            predicates.add(criteriaBuilder.equal(groupJoin.get(Group_.id), filter.getGroupId()));
        }

        if (filter.getTeacherId() != null) {
            predicates.add(criteriaBuilder.equal(teacherJoin.get(Teacher_.id), filter.getTeacherId()));
        }

        if (filter.getClassroomId() != null) {
            predicates.add(criteriaBuilder.equal(classroomJoin.get(Classroom_.id), filter.getClassroomId()));
        }

        if (filter.getDayNumber() != null) {
            predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(criteriaBuilder.trim(root.get(Timetable_.dayName))), Constants.TIMETABLE_DAYS_NAME.get(filter.getDayNumber()).trim().toLowerCase()));
        }

        if (filter.getWeekNumber() != null) {
            predicates.add(criteriaBuilder.equal(root.get(Timetable_.weekNumber), filter.getWeekNumber()));
        }

        if (filter.getSemesterNumber() != null) {
            predicates.add(criteriaBuilder.equal(semesterJoin.get(Semester_.semesterNumber), filter.getSemesterNumber()));
        }

        if (filter.getWeekDate() != null) {
            Semester semester = semesterService.determineSemesterByWeekDate(filter.getTenant(), filter.getWeekDate());
            long weeksBetween = ChronoUnit.WEEKS.between(semester.getDate().with(DayOfWeek.MONDAY), filter.getWeekDate().with(DayOfWeek.MONDAY));
            predicates.add(criteriaBuilder.equal(root.get(Timetable_.weekNumber), Math.toIntExact(weeksBetween) + 1));
            predicates.add(criteriaBuilder.equal(semesterJoin.get(Semester_.semesterNumber), semester.getSemesterNumber()));
        }
//todo remove
//        if (filter.getCheckerInfo() != null) {
//            CheckerInfo info = filter.getCheckerInfo();
//
//            if (info.getSemester().equals(info.getYearInfo().getFirst())) {
//                predicates.add(criteriaBuilder.or(
//                        criteriaBuilder.and(
//                                criteriaBuilder.equal(semesterJoin.get(Semester_.id), info.getYearInfo().getFirst().getId()),
//                                criteriaBuilder.greaterThanOrEqualTo(root.get(Timetable_.weekNumber), info.getWeek())
//                        ),
//                        criteriaBuilder.equal(semesterJoin.get(Semester_.id), info.getYearInfo().getSecond().getId())
//                ));
//            } else {
//                predicates.add(criteriaBuilder.and(
//                        criteriaBuilder.equal(semesterJoin.get(Semester_.id), info.getSemester().getId()),
//                        criteriaBuilder.greaterThanOrEqualTo(root.get(Timetable_.weekNumber), info.getWeek())
//                ));
//            }
//        }

        query.where(predicates.toArray(new Predicate[0]));
        query.orderBy(criteriaBuilder.asc(semesterJoin.get(Semester_.semesterNumber)), criteriaBuilder.asc(root.get(Timetable_.weekNumber)),
                criteriaBuilder.asc(root.get(Timetable_.dayName)), criteriaBuilder.asc(root.get(Timetable_.lessonNumber)));
        return findTenantAll(filter.getTenant(), query, buildEntityGraph());
    }

    private EntityGraph<Timetable> buildEntityGraph() {
        EntityGraph<Timetable> entityGraph = entityManager.createEntityGraph(Timetable.class);
        entityGraph.addSubgraph(Timetable_.classroom);

        Subgraph<Lesson> lesson = entityGraph.addSubgraph(Timetable_.lesson);
        lesson.addSubgraph(Lesson_.group);

        Subgraph<Torrent> torrent = lesson.addSubgraph(Lesson_.torrent);
        torrent.addSubgraph(Torrent_.discipline);
        torrent.addSubgraph(Torrent_.teacher);
        torrent.addSubgraph(Torrent_.semester);
        return entityGraph;
    }
}
