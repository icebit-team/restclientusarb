package com.usarb.restclient.service;

import com.usarb.restclient.model.Discipline;
import com.usarb.restclient.model.Discipline_;
import com.usarb.restclient.repository.DisciplineRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class DisciplineService extends TenantService<Discipline> {

    @PersistenceContext
    private EntityManager entityManager;

    public DisciplineService(DisciplineRepository repository) {
        super(repository);
    }

    public List<Discipline> findCriteriaAll(String partOfName) {
        return findCriteriaAll(null, partOfName);
    }

    public List<Discipline> findCriteriaAll(String tenant, String partOfName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Discipline> query = criteriaBuilder.createQuery(Discipline.class);
        Root<Discipline> root = query.from(Discipline.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.isNotNull(root.get(Discipline_.name)));

        if (partOfName != null) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(Discipline_.name)), String.format("%%%s%%", partOfName).toLowerCase()));
        }

        query.where(predicates.toArray(new Predicate[0]));
        return findTenantAll(tenant, query);
    }
}
