package com.usarb.restclient.service;

import com.usarb.restclient.model.TenantEntity;
import com.usarb.restclient.tenant.TenantContext;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.usarb.restclient.utils.Constants.JAVAX_PERSISTENCE_FETCH_GRAPH;

public abstract class TenantService<T extends TenantEntity> {

    @PersistenceContext
    private EntityManager entityManager;

    private JpaRepository<T, Long> repository;

    public TenantService(JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    public List<T> findTenantAll() {
        TenantContext.setLicenseTenant();
        List<T> result = new ArrayList<>(repository.findAll());

        TenantContext.setMasterTenant();
        result.addAll(repository.findAll());

        return result.stream().sorted(Comparator.comparing(T::getId)).collect(Collectors.toList());
    }

    public List<T> findTenantAll(String tenant) {
        TenantContext.setTenant(tenant);
        return repository.findAll();
    }

    public T findTenantOne(String tenant, Long id) {
        TenantContext.setTenant(tenant);
        return repository.findById(id).orElse(null);
    }

    private List<T> findTenantAll(CriteriaQuery<T> query, EntityGraph<T> entityGraph) {
        TenantContext.setLicenseTenant();
        List<T> result = new ArrayList<>(entityManager.createQuery(query).setHint(JAVAX_PERSISTENCE_FETCH_GRAPH, entityGraph).getResultList());

        TenantContext.setMasterTenant();
        result.addAll(entityManager.createQuery(query).setHint(JAVAX_PERSISTENCE_FETCH_GRAPH, entityGraph).getResultList());

        return result.stream().sorted(Comparator.comparing(T::getId)).collect(Collectors.toList());
    }

    private List<T> findTenantAll(CriteriaQuery<T> query) {
        TenantContext.setLicenseTenant();
        List<T> result = new ArrayList<>(entityManager.createQuery(query).getResultList());

        TenantContext.setMasterTenant();
        result.addAll(entityManager.createQuery(query).getResultList());

        return result.stream().sorted(Comparator.comparing(T::getId)).collect(Collectors.toList());
    }

    List<T> findTenantAll(String tenant, CriteriaQuery<T> query, EntityGraph<T> entityGraph) {
        if (tenant != null) {
            TenantContext.setTenant(tenant);
            return entityManager.createQuery(query).setHint(JAVAX_PERSISTENCE_FETCH_GRAPH, entityGraph).getResultList();
        } else {
            return findTenantAll(query, entityGraph);
        }
    }

    List<T> findTenantAll(String tenant, CriteriaQuery<T> query) {
        if (tenant != null) {
            TenantContext.setTenant(tenant);
            return entityManager.createQuery(query).getResultList();
        } else {
            return findTenantAll(query);
        }
    }

    T findTenantOne(String tenant, CriteriaQuery<T> query, EntityGraph<T> entityGraph) {
        TenantContext.setTenant(tenant);
        return entityManager.createQuery(query).setHint(JAVAX_PERSISTENCE_FETCH_GRAPH, entityGraph).getSingleResult();
    }
}
