package com.usarb.restclient.service;

import com.usarb.restclient.model.Semester;
import com.usarb.restclient.repository.SemesterRepository;
import com.usarb.restclient.tenant.TenantContext;
import com.usarb.restclient.utils.UnsupportedDateException;
import com.usarb.restclient.utils.YearInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class SemesterService extends TenantService<Semester> {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Resource
    private SemesterRepository semesterRepository;

    public SemesterService(SemesterRepository repository) {
        super(repository);
    }

    public Integer determineWeekNumber(Semester semester, LocalDate localDate) {
        long weeksBetween = ChronoUnit.WEEKS.between(semester.getDate().with(DayOfWeek.MONDAY), localDate.with(DayOfWeek.MONDAY));
        return Math.toIntExact(weeksBetween) + 1;
    }

    public YearInfo determineYear(String tenant) {
        if (tenant == null) {
            TenantContext.setLicenseTenant();
        } else {
            TenantContext.setTenant(tenant);
        }

        List<Semester> semesters = semesterRepository.findAll();

        Semester first = semesters.stream()
                .filter(semester -> semester.getSemesterNumber().equals(1))
                .reduce((semester, semester2) -> semester.getDate().isAfter(semester2.getDate()) ? semester : semester2)
                .orElse(null);

        Semester second = semesters.stream()
                .filter(semester -> semester.getSemesterNumber().equals(2))
                .reduce((semester, semester2) -> semester.getDate().isAfter(semester2.getDate()) ? semester : semester2)
                .orElse(null);

        if (first == null || second == null || first.getDate().isAfter(second.getDate())) {
            throw new RuntimeException("Wrong semesters");
        }

        return new YearInfo(first, second);
    }

    public Semester determineSemesterByWeekDate(YearInfo yearInfo, LocalDate weekDate) {
        if (weekDate.isBefore(yearInfo.getFirst().getDate())) {
            throw new UnsupportedDateException(String.format("Unsupported date. Expected date between %s and %s.",
                    yearInfo.getFirst().getDate().format(DATE_FORMATTER), yearInfo.getSecond().getDate().format(DATE_FORMATTER)));
        }

        return weekDate.isAfter(yearInfo.getFirst().getDate().minusDays(1)) &&
                weekDate.isBefore(yearInfo.getSecond().getDate().plusDays(1)) ? yearInfo.getFirst() : yearInfo.getSecond();
    }

    public Semester determineSemesterByWeekDate(String tenant, LocalDate weekDate) {
        YearInfo yearInfo = determineYear(tenant);
        return determineSemesterByWeekDate(yearInfo, weekDate);
    }
}
