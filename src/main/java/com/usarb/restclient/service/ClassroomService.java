package com.usarb.restclient.service;

import com.usarb.restclient.model.Classroom;
import com.usarb.restclient.model.Classroom_;
import com.usarb.restclient.repository.ClassroomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassroomService extends TenantService<Classroom> {

    @PersistenceContext
    private EntityManager entityManager;

    public ClassroomService(ClassroomRepository repository) {
        super(repository);
    }

    public List<Classroom> findCriteriaAll(String tenant, String partOfName, Boolean hasTimetable) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Classroom> query = criteriaBuilder.createQuery(Classroom.class);

        Root<Classroom> root = query.from(Classroom.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.isNotNull(root.get(Classroom_.name)));

        if (partOfName != null) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(Classroom_.name)), String.format("%%%s%%", partOfName).toLowerCase()));
        }

        if (hasTimetable != null) {
            if (Boolean.TRUE.equals(hasTimetable)) {
                predicates.add(criteriaBuilder.isNotEmpty(root.get(Classroom_.timetables)));
            } else {
                predicates.add(criteriaBuilder.isEmpty(root.get(Classroom_.timetables)));
            }
        }

        query.where(predicates.toArray(new Predicate[0]));
        return findTenantAll(tenant, query).stream().sorted(Comparator.comparing(Classroom::getNameStr)).distinct().collect(Collectors.toList());
    }
}
