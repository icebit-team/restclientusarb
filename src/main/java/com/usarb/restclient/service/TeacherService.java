package com.usarb.restclient.service;

import com.usarb.restclient.model.Teacher;
import com.usarb.restclient.model.Teacher_;
import com.usarb.restclient.repository.TeacherRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherService extends TenantService<Teacher> {

    @PersistenceContext
    private EntityManager entityManager;

    public TeacherService(TeacherRepository repository) {
        super(repository);
    }

    public List<Teacher> findCriteriaAll(String tenant, String partOfName, Boolean hasTimetable) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Teacher> query = criteriaBuilder.createQuery(Teacher.class);

        Root<Teacher> root = query.from(Teacher.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.isNotNull(root.get(Teacher_.name)));

        if (partOfName != null) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(Teacher_.name)), String.format("%%%s%%", partOfName).toLowerCase()));
        }

        if (hasTimetable != null) {
            if (Boolean.TRUE.equals(hasTimetable)) {
                predicates.add(criteriaBuilder.isNotEmpty(root.get(Teacher_.torrents)));
            } else {
                predicates.add(criteriaBuilder.isEmpty(root.get(Teacher_.torrents)));
            }
        }

        query.where(predicates.toArray(new Predicate[0]));
        return findTenantAll(tenant, query).stream().sorted(Comparator.comparing(Teacher::getNameStr)).distinct().collect(Collectors.toList());
    }
}
