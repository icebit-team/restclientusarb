package com.usarb.restclient.service;

import com.usarb.restclient.model.Speciality;
import com.usarb.restclient.model.Speciality_;
import com.usarb.restclient.repository.SpecialityRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class SpecialityService extends TenantService<Speciality> {

    @PersistenceContext
    private EntityManager entityManager;

    public SpecialityService(SpecialityRepository repository) {
        super(repository);
    }

    public List<Speciality> findCriteriaAll() {
        return findCriteriaAll(null);
    }

    public List<Speciality> findCriteriaAll(String tenant) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Speciality> query = criteriaBuilder.createQuery(Speciality.class);
        query.from(Speciality.class);
        return findTenantAll(tenant, query, buildEntityGraph());
    }

    public Speciality findCriteriaById(String tenant, Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Speciality> query = criteriaBuilder.createQuery(Speciality.class);

        Root<Speciality> root = query.from(Speciality.class);
        query.where(criteriaBuilder.equal(root.get(Speciality_.id), id));

        return findTenantOne(tenant, query, buildEntityGraph());
    }

    private EntityGraph<Speciality> buildEntityGraph() {
        EntityGraph<Speciality> entityGraph = entityManager.createEntityGraph(Speciality.class);
        entityGraph.addSubgraph(Speciality_.genericArea);
        entityGraph.addSubgraph(Speciality_.professionalArea);
        entityGraph.addSubgraph(Speciality_.shareOfPlanUnits);
        return entityGraph;
    }
}
