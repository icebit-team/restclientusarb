package com.usarb.restclient.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usarb.restclient.dto.FirebaseNotificationType;
import com.usarb.restclient.dto.FirebaseRequestContentDto;
import com.usarb.restclient.dto.GenericTimetableDto;
import com.usarb.restclient.schedule.ChangesInfo;
import com.usarb.restclient.schedule.CheckerEditItemWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class FirebaseService {

    @Value("${restclient.firebase.key}")
    private String firebaseKey;

    @Value("${restclient.firebase.url}")
    private String firebaseUrl;

    public void notify(ChangesInfo changes, boolean group, boolean teacher, boolean classroom) throws JsonProcessingException {
        for (Map.Entry<String, List<GenericTimetableDto>> stringListEntry : changes.getNewItems().entrySet()) {
            for (Map.Entry<Integer, List<GenericTimetableDto>> entry : splitList(stringListEntry.getValue()).entrySet()) {
                if (!entry.getValue().isEmpty()) {
                    request(new FirebaseRequestContentDto(stringListEntry.getKey(), entry.getValue(), group, teacher, classroom, FirebaseNotificationType.NEW));
                }
            }
        }

        for (Map.Entry<String, List<GenericTimetableDto>> stringListEntry : changes.getDeleteItems().entrySet()) {
            for (Map.Entry<Integer, List<GenericTimetableDto>> entry : splitList(stringListEntry.getValue()).entrySet()) {
                if (!entry.getValue().isEmpty()) {
                    request(new FirebaseRequestContentDto(stringListEntry.getKey(), entry.getValue(), group, teacher, classroom, FirebaseNotificationType.DELETE));
                }
            }
        }

        for (Map.Entry<String, List<CheckerEditItemWrapper>> stringListEntry : changes.getEditItems().entrySet()) {
            for (CheckerEditItemWrapper wrap : stringListEntry.getValue()) {
                request(new FirebaseRequestContentDto(stringListEntry.getKey(), wrap, group, teacher, classroom));
            }
        }
    }

    private void request(FirebaseRequestContentDto firebaseRequestContentDto) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "key=" + firebaseKey);

        ObjectMapper objectMapper = new ObjectMapper();
        String content = objectMapper.writeValueAsString(firebaseRequestContentDto);

        HttpEntity<String> httpEntity = new HttpEntity<>(content, headers);

        try {
            new RestTemplate().postForEntity(firebaseUrl, httpEntity, String.class);
        } catch (Exception e) {
            log.error(String.format("Firebase ERROR - `%s`", firebaseRequestContentDto.getData().getTopicInfo()), e);
        }
    }

    private static <T> Map<Integer, List<T>> splitList(List<T> list) {
        Map<Integer, List<T>> result = new HashMap<>();

        int i = 0;
        while (i <= list.size()) {
            if (list.size() > i + 5) {
                result.put(i, list.subList(i, i + 5));
            } else {
                result.put(i, list.subList(i, list.size()));
            }

            i = i + 5;
        }

        return result;
    }
}
