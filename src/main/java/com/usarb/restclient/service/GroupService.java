package com.usarb.restclient.service;

import com.usarb.restclient.model.Group;
import com.usarb.restclient.model.Group_;
import com.usarb.restclient.repository.GroupRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupService extends TenantService<Group> {

    @PersistenceContext
    private EntityManager entityManager;

    public GroupService(GroupRepository repository) {
        super(repository);
    }

    public List<Group> findCriteriaAll(String tenant, Integer year, String partOfName, Boolean hasTimetable) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> query = criteriaBuilder.createQuery(Group.class);

        Root<Group> root = query.from(Group.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.isNotNull(root.get(Group_.name)));

        if (year != null) {
            predicates.add(criteriaBuilder.equal(root.get(Group_.year), year));
        }

        if (partOfName != null) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(Group_.name)), String.format("%%%s%%", partOfName).toLowerCase()));
        }

        if (hasTimetable != null) {
            if (Boolean.TRUE.equals(hasTimetable)) {
                predicates.add(criteriaBuilder.isNotEmpty(root.get(Group_.lessons)));
            } else {
                predicates.add(criteriaBuilder.isEmpty(root.get(Group_.lessons)));
            }
        }

        query.where(predicates.toArray(new Predicate[0]));
        return findTenantAll(tenant, query).stream().sorted(Comparator.comparing(Group::getNameStr)).distinct().collect(Collectors.toList()) ;
    }
}
