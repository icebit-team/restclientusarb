package com.usarb.restclient.utils;

import com.usarb.restclient.schedule.CheckerInfo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TimetableFilter {

    private String tenant;
    private Long id;
    private String groupName;
    private String teacherName;
    private String classroomName;
    private Long groupId;
    private Long teacherId;
    private Long classroomId;
    private Integer weekNumber;
    private Integer dayNumber;
    private Integer semesterNumber;
    private LocalDate weekDate;

    private Integer semesterStartInterval;
    private Integer semesterEndInterval;
    private Integer weekStartInterval;
    private Integer weekEndInterval;

    private CheckerInfo checkerInfo;

    public static TimetableFilter buildDefault(String tenant, Long id, String groupName, String teacherName, String classroomName, Long groupId, Long teacherId, Long classroomId,
                                               Integer weekNumber, Integer dayNumber, Integer semesterNumber, LocalDate weekDate) {
        TimetableFilter filter = new TimetableFilter();
        filter.setTenant(tenant);
        filter.setId(id);
        filter.setGroupName(groupName);
        filter.setTeacherName(teacherName);
        filter.setClassroomName(classroomName);
        filter.setGroupId(groupId);
        filter.setTeacherId(teacherId);
        filter.setClassroomId(classroomId);
        filter.setWeekNumber(weekNumber);
        filter.setDayNumber(dayNumber);
        filter.setSemesterNumber(semesterNumber);
        filter.setWeekDate(weekDate);
        return filter;
    }

    public static TimetableFilter build(CheckerInfo checkerInfo) {
        TimetableFilter filter = new TimetableFilter();
        filter.setTenant(Constants.TENANT_LICENSE); //todo remove
        filter.setCheckerInfo(checkerInfo);
        return filter;
    }
}
