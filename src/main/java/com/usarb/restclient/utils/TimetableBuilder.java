package com.usarb.restclient.utils;

import com.usarb.restclient.dto.GenericTimetableDto;
import com.usarb.restclient.model.Timetable;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TimetableBuilder {

    public static List<GenericTimetableDto> buildGenericTimetableResponse(List<Timetable> timetables, Function<Map.Entry<Integer, List<Timetable>>, GenericTimetableDto> builder) {
        return timetables.stream().collect(Collectors.groupingBy(Timetable::getDate))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream()
                        .collect(Collectors.groupingBy(Timetable::getLessonNumber)))).entrySet().stream()
                .flatMap(dateEntry -> dateEntry.getValue().entrySet().stream().map(builder)
                        .collect(Collectors.toList()).stream())
                .sorted(Comparator.comparing(GenericTimetableDto::getDate).thenComparing(GenericTimetableDto::getLessonNumber))
                .collect(Collectors.toList());
    }
}
