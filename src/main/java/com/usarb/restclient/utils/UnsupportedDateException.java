package com.usarb.restclient.utils;

public class UnsupportedDateException extends RuntimeException {
    public UnsupportedDateException(String message) {
        super(message);
    }
}
