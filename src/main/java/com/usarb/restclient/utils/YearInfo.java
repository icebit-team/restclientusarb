package com.usarb.restclient.utils;

import com.usarb.restclient.model.Semester;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class YearInfo {

    private Semester first;

    private Semester second;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearInfo yearInfo = (YearInfo) o;
        return first.equals(yearInfo.first) &&
                second.equals(yearInfo.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
