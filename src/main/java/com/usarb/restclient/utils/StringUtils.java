package com.usarb.restclient.utils;

public class StringUtils {

    public static String replaceRoChars(String value) {
        return value.replaceAll("[\u0103\u00e2]", "a") //ăâ
                .replaceAll("[\u0102\u00c2]", "A") //ĂÂ
                .replaceAll("\u00ee", "i") //î
                .replaceAll("\u00ce", "I") //Î
                .replaceAll("[\u0219\u015f]", "s") //șş
                .replaceAll("[\u0218\u015e]", "S") //ȘŞ
                .replaceAll("[\u021b\u0163]", "t") //țţ
                .replaceAll("[\u021a\u0162]", "T"); //ȚŢ
    }

    public static String buildFirebaseTopic(String value) {
        return replaceRoChars(value.toUpperCase()).replaceAll("\\s+", "_").replaceAll("\\W+", "");
    }
}
