package com.usarb.restclient.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Constants {
    public static final String JAVAX_PERSISTENCE_FETCH_GRAPH = "javax.persistence.fetchgraph";

    public static final Map<Integer, String> TIMETABLE_DAYS_NAME = new HashMap<>();
    public static final Map<String, Integer> TIMETABLE_DAYS_NUMBER;

    public static final String TENANT_LICENSE = "license";
    public static final String TENANT_MASTER = "master";

    static {
        TIMETABLE_DAYS_NAME.put(1, "Luni");
        TIMETABLE_DAYS_NAME.put(2, "Marti");
        TIMETABLE_DAYS_NAME.put(3, "Miercuri");
        TIMETABLE_DAYS_NAME.put(4, "Joi");
        TIMETABLE_DAYS_NAME.put(5, "Vineri");
        TIMETABLE_DAYS_NAME.put(6, "Simbata");

        TIMETABLE_DAYS_NUMBER = TIMETABLE_DAYS_NAME.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }
}