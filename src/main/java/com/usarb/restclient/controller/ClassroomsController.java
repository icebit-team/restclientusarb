package com.usarb.restclient.controller;

import com.usarb.restclient.model.Classroom;
import com.usarb.restclient.service.ClassroomService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/classrooms")
public class ClassroomsController {

    @Resource
    private ClassroomService classroomService;

    @GetMapping
    public List<Classroom> loadAllClassroom(@RequestParam(required = false) String partOfName, @RequestParam(required = false) Boolean hasTimetable) {
        return classroomService.findCriteriaAll(null, partOfName, hasTimetable);
    }

    @GetMapping("/{tenant}")
    public List<Classroom> loadAllClassroom(@PathVariable String tenant, @RequestParam(required = false) String partOfName, @RequestParam(required = false) Boolean hasTimetable) {
        return classroomService.findCriteriaAll(tenant, partOfName, hasTimetable);
    }

    @GetMapping("/{tenant}/{id}")
    public Classroom loadClassroom(@PathVariable String tenant, @PathVariable Long id) {
        return classroomService.findTenantOne(tenant, id);
    }
}
