package com.usarb.restclient.controller;

import com.usarb.restclient.model.Faculty;
import com.usarb.restclient.service.FacultyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/faculties")
public class FacultiesController {

    @Resource
    private FacultyService facultyService;

    @GetMapping
    public List<Faculty> loadAllFaculty() {
        return facultyService.findTenantAll();
    }

    @GetMapping("/{tenant}")
    public List<Faculty> loadTenantFaculty(@PathVariable String tenant) {
        return facultyService.findTenantAll(tenant);
    }

    @GetMapping("/{tenant}/{id}")
    public Faculty loadFaculty(@PathVariable String tenant, @PathVariable Long id) {
        return facultyService.findTenantOne(tenant, id);
    }
}
