package com.usarb.restclient.controller;

import com.usarb.restclient.model.Teacher;
import com.usarb.restclient.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/teachers")
public class TeachersController {

    @Resource
    private TeacherService teacherService;

    @GetMapping
    public List<Teacher> loadAllTimetableTeacher(@RequestParam(required = false) String partOfName,
                                                 @RequestParam(required = false) Boolean active,
                                                 @RequestParam(required = false) Boolean hasTimetable) {
        List<Teacher> result = teacherService.findCriteriaAll(null, partOfName, hasTimetable);
        return active == null ? result : result.stream().filter(teacher -> teacher.getActive().equals(active)).collect(Collectors.toList());
    }

    @GetMapping("/{tenant}")
    public List<Teacher> loadAllTimetableTeacher(@PathVariable String tenant,
                                                 @RequestParam(required = false) String partOfName,
                                                 @RequestParam(required = false) Boolean active,
                                                 @RequestParam(required = false) Boolean hasTimetable) {
        List<Teacher> result = teacherService.findCriteriaAll(tenant, partOfName, hasTimetable);
        return active == null ? result : result.stream().filter(teacher -> teacher.getActive().equals(active)).collect(Collectors.toList());
    }

    @GetMapping("/{tenant}/{id}")
    public Teacher loadTimetableTeacher(@PathVariable String tenant, @PathVariable Long id) {
        return teacherService.findTenantOne(tenant, id);
    }
}
