package com.usarb.restclient.controller;

import com.usarb.restclient.model.Speciality;
import com.usarb.restclient.service.SpecialityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/specialities")
public class SpecialitiesController {

    @Resource
    private SpecialityService specialityService;

    @GetMapping
    public List<Speciality> loadAllSpeciality() {
        return specialityService.findCriteriaAll();
    }

    @GetMapping("/{tenant}")
    public List<Speciality> loadTenantSpeciality(@PathVariable String tenant) {
        return specialityService.findCriteriaAll(tenant);
    }

    @GetMapping("/{tenant}/{id}")
    public Speciality loadSpeciality(@PathVariable String tenant, @PathVariable Long id) {
        return specialityService.findCriteriaById(tenant, id);
    }
}
