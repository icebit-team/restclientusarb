package com.usarb.restclient.controller;

import com.usarb.restclient.model.Semester;
import com.usarb.restclient.service.SemesterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/semesters")
public class SemestersController {

    @Resource
    private SemesterService semesterService;

    @GetMapping
    public List<Semester> loadAllSemesters() {
        return semesterService.findTenantAll();
    }

    @GetMapping("/{tenant}")
    public List<Semester> loadTenantSemesters(@PathVariable String tenant) {
        return semesterService.findTenantAll(tenant);
    }
}
