package com.usarb.restclient.controller;

import com.usarb.restclient.model.Group;
import com.usarb.restclient.service.GroupService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/groups")
public class GroupsController {

    @Resource
    private GroupService groupService;

    @GetMapping
    public List<Group> loadAllGroup(@RequestParam(required = false) Integer year, @RequestParam(required = false) String partOfName, @RequestParam(required = false) Boolean hasTimetable) {
        return groupService.findCriteriaAll(null, year, partOfName, hasTimetable);
    }

    @GetMapping("/{tenant}")
    public List<Group> loadTenantGroups(@PathVariable String tenant, @RequestParam(required = false) Integer year, @RequestParam(required = false) String partOfName, @RequestParam(required = false) Boolean hasTimetable) {
        return groupService.findCriteriaAll(tenant, year, partOfName, hasTimetable);
    }

    @GetMapping("/{tenant}/{id}")
    public Group loadGroup(@PathVariable String tenant, @PathVariable Long id) {
        return groupService.findTenantOne(tenant, id);
    }

}
