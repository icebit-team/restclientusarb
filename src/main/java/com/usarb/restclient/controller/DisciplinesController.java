package com.usarb.restclient.controller;

import com.usarb.restclient.model.Discipline;
import com.usarb.restclient.service.DisciplineService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/disciplines")
public class DisciplinesController {

    @Resource
    private DisciplineService disciplineService;

    @GetMapping
    public List<Discipline> loadAllDisciplines(@RequestParam(required = false) String partOfName) {
        return disciplineService.findCriteriaAll(partOfName);
    }

    @GetMapping("/{tenant}")
    public List<Discipline> loadTenantDisciplines(@PathVariable String tenant, @RequestParam(required = false) String partOfName) {
        return disciplineService.findCriteriaAll(tenant, partOfName);
    }

    @GetMapping("/{tenant}/{id}")
    public Discipline loadDiscipline(@PathVariable String tenant, @PathVariable Long id) {
        return disciplineService.findTenantOne(tenant, id);
    }
}
