package com.usarb.restclient.controller;

import com.usarb.restclient.model.Department;
import com.usarb.restclient.service.DepartmentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentsController {

    @Resource
    private DepartmentService departmentService;

    @GetMapping
    public List<Department> loadAllDepartments() {
        return departmentService.findTenantAll();
    }

    @GetMapping("/{tenant}")
    public List<Department> loadTenantDepartments(@PathVariable String tenant) {
        return departmentService.findTenantAll(tenant);
    }

    @GetMapping("/{tenant}/{id}")
    public Department loadDepartment(@PathVariable String tenant, @PathVariable Long id) {
        return departmentService.findTenantOne(tenant, id);
    }
}
