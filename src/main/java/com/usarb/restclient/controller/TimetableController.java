package com.usarb.restclient.controller;

import com.usarb.restclient.dto.GenericTimetableDto;
import com.usarb.restclient.dto.TimetableDto;
import com.usarb.restclient.dto.TimetableHoursPerDateDto;
import com.usarb.restclient.model.Timetable;
import com.usarb.restclient.service.TimetableService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.usarb.restclient.utils.TimetableBuilder.buildGenericTimetableResponse;

@RestController
@RequestMapping(value = "/timetable")
public class TimetableController {

    @Resource
    private TimetableService timetableService;

    @GetMapping
    public List<Timetable> loadAllTimetable(@RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                            @RequestParam(required = false) Integer dayNumber, @RequestParam(required = false) Integer semesterNumber, @RequestParam(required = false) Integer weekNumber,
                                            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(null, null, groupName, teacherName, classroomName, null, null, null, weekNumber, dayNumber, semesterNumber, weekDate);
    }

    @GetMapping("/{tenant}")
    public List<Timetable> loadAllTimetable(@PathVariable String tenant, @RequestParam(required = false) Long id,
                                            @RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                            @RequestParam(required = false) Long groupId, @RequestParam(required = false) Long teacherId, @RequestParam(required = false) Long classroomId,
                                            @RequestParam(required = false) Integer dayNumber, @RequestParam(required = false) Integer semesterNumber,
                                            @RequestParam(required = false) Integer weekNumber, @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(tenant, id, groupName, teacherName, classroomName, groupId, teacherId, classroomId, weekNumber, dayNumber, semesterNumber, weekDate);
    }

    @GetMapping(value = "/short")
    public List<TimetableDto> loadAllShortTimetable(@RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                                    @RequestParam(required = false) Integer dayNumber, @RequestParam(required = false) Integer semesterNumber, @RequestParam(required = false) Integer weekNumber,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(null, null, groupName, teacherName, classroomName, null, null, null, weekNumber, dayNumber, semesterNumber, weekDate).stream()
                .map(TimetableDto::new).sorted(Comparator.comparing(TimetableDto::getDate).thenComparing(TimetableDto::getLessonNumber)).collect(Collectors.toList());
    }

    @GetMapping(value = "/{tenant}/short")
    public List<TimetableDto> loadAllShortTimetable(@PathVariable String tenant, @RequestParam(required = false) Long id,
                                                    @RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                                    @RequestParam(required = false) Long groupId, @RequestParam(required = false) Long teacherId, @RequestParam(required = false) Long classroomId,
                                                    @RequestParam(required = false) Integer dayNumber,
                                                    @RequestParam(required = false) Integer semesterNumber, @RequestParam(required = false) Integer weekNumber,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(tenant, id, groupName, teacherName, classroomName, groupId, teacherId, classroomId, weekNumber, dayNumber, semesterNumber, weekDate).stream()
                .map(TimetableDto::new).sorted(Comparator.comparing(TimetableDto::getDate).thenComparing(TimetableDto::getLessonNumber)).collect(Collectors.toList());
    }

    @GetMapping(value = "/hours-per-date")
    public List<TimetableHoursPerDateDto> loadAllTimetableHoursPerDate(@RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                                                       @RequestParam(required = false) Integer dayNumber, @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(null, null, groupName, teacherName, classroomName, null, null, null, null, dayNumber, null, weekDate).stream()
                .collect(Collectors.groupingBy(Timetable::getDate))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream()
                        .map(Timetable::getLessonNumber).distinct().count())).entrySet().stream()
                .map(entry -> new TimetableHoursPerDateDto(entry.getValue(), entry.getKey()))
                .sorted(Comparator.comparing(TimetableHoursPerDateDto::getDate))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{tenant}/hours-per-date")
    public List<TimetableHoursPerDateDto> loadAllTimetableHoursPerDate(@PathVariable String tenant,
                                                                       @RequestParam(required = false) String groupName, @RequestParam(required = false) String teacherName, @RequestParam(required = false) String classroomName,
                                                                       @RequestParam(required = false) Long groupId, @RequestParam(required = false) Long teacherId, @RequestParam(required = false) Long classroomId,
                                                                       @RequestParam(required = false) Integer dayNumber, @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        return timetableService.findCriteriaAll(tenant, null, groupName, teacherName, classroomName, groupId, teacherId, classroomId, null, dayNumber, null, weekDate).stream()
                .collect(Collectors.groupingBy(Timetable::getDate))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream()
                        .map(Timetable::getLessonNumber).distinct().count())).entrySet().stream()
                .map(entry -> new TimetableHoursPerDateDto(entry.getValue(), entry.getKey()))
                .sorted(Comparator.comparing(TimetableHoursPerDateDto::getDate))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/teacher")
    public List<GenericTimetableDto> loadAllTimetableForTeacher(@RequestParam String teacherName, @RequestParam(required = false) Integer dayNumber,
                                                                @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        List<Timetable> timetables = timetableService.findCriteriaAll(null, null, null, teacherName, null, null, null, null, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForTeacher(lessonEntry.getValue()));
    }

    @GetMapping(value = "/{tenant}/teacher")
    public List<GenericTimetableDto> loadAllTimetableForTeacher(@PathVariable String tenant,
                                                                @RequestParam(required = false) Long teacherId, @RequestParam(required = false) String teacherName, @RequestParam(required = false) Integer dayNumber,
                                                                @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        Assert.isTrue(teacherId != null || teacherName != null, "Teacher 'name' or 'id' is required");
        List<Timetable> timetables = timetableService.findCriteriaAll(tenant, null, null, teacherName, null, null, teacherId, null, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForTeacher(lessonEntry.getValue()));
    }

    @GetMapping(value = "/group")
    public List<GenericTimetableDto> loadAllTimetableForGroup(@RequestParam String groupName, @RequestParam(required = false) Integer dayNumber,
                                                              @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        List<Timetable> timetables = timetableService.findCriteriaAll(null, null, groupName, null, null, null, null, null, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForGroup(lessonEntry.getValue()));
    }

    @GetMapping(value = "/{tenant}/group")
    public List<GenericTimetableDto> loadAllTimetableForGroup(@PathVariable String tenant,
                                                              @RequestParam(required = false) Long groupId, @RequestParam(required = false) String groupName, @RequestParam(required = false) Integer dayNumber,
                                                              @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        Assert.isTrue(groupId != null || groupName != null, "Group 'name' or 'id' is required");
        List<Timetable> timetables = timetableService.findCriteriaAll(tenant, null, groupName, null, null, groupId, null, null, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForGroup(lessonEntry.getValue()));
    }

    @GetMapping(value = "/classroom")
    public List<GenericTimetableDto> loadAllTimetableForClassroom(@RequestParam String classroomName, @RequestParam(required = false) Integer dayNumber,
                                                                  @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        List<Timetable> timetables = timetableService.findCriteriaAll(null, null, null, null, classroomName, null, null, null, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForClassroom(lessonEntry.getValue()));
    }

    @GetMapping(value = "/{tenant}/classroom")
    public List<GenericTimetableDto> loadAllTimetableForClassroom(@PathVariable String tenant,
                                                                  @RequestParam(required = false) Long classroomId, @RequestParam(required = false) String classroomName, @RequestParam(required = false) Integer dayNumber,
                                                                  @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate weekDate) {
        Assert.isTrue(classroomId != null || classroomName != null, "Classroom 'name' or 'id' is required");
        List<Timetable> timetables = timetableService.findCriteriaAll(tenant, null, null, null, classroomName, null, null, classroomId, null, dayNumber, null, weekDate);
        return buildGenericTimetableResponse(timetables, lessonEntry -> GenericTimetableDto.instanceForClassroom(lessonEntry.getValue()));
    }
}
