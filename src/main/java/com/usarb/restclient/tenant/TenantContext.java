package com.usarb.restclient.tenant;

import com.usarb.restclient.utils.Constants;

public class TenantContext {

    private static ThreadLocal<String> threadTenant = new ThreadLocal<>();

    public static void setTenant(String tenant) {
        threadTenant.set(tenant);
    }

    public static void setLicenseTenant() {
        setTenant(Constants.TENANT_LICENSE);
    }

    public static void setMasterTenant() {
        setTenant(Constants.TENANT_MASTER);
    }

    public static String getTenant() {
        return threadTenant.get();
    }
}
