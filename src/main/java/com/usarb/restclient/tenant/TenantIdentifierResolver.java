package com.usarb.restclient.tenant;

import com.usarb.restclient.utils.Constants;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

@Component
public class TenantIdentifierResolver implements CurrentTenantIdentifierResolver {

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = TenantContext.getTenant();

        if (tenantId != null) {
            return tenantId;
        }

        return Constants.TENANT_LICENSE;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
