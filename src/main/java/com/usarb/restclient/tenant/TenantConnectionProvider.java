package com.usarb.restclient.tenant;

import com.usarb.restclient.utils.ApplicationException;
import com.usarb.restclient.utils.Constants;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

@Component
public class TenantConnectionProvider implements MultiTenantConnectionProvider {

    @Resource
    private Map<String, DataSource> dataSourceMap;

    @Override
    public Connection getAnyConnection() throws SQLException {
        return dataSourceMap.get(Constants.TENANT_LICENSE).getConnection();
    }

    @Override
    public void releaseAnyConnection(Connection connection) throws SQLException {
        connection.close();
    }

    @Override
    public Connection getConnection(String tenant) throws SQLException {
        DataSource dataSource = dataSourceMap.get(tenant);

        if (dataSource == null) {
            throw new ApplicationException(String.format("Undefined TENANT: '%s'. Expected '%s' or '%s'", tenant, Constants.TENANT_LICENSE, Constants.TENANT_MASTER));
        }

        return dataSource.getConnection();
    }

    @Override
    public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
        connection.close();
    }

    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> unwrapType) {
        return null;
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return true;
    }
}
