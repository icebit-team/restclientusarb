package com.usarb.restclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RestClientUsarbApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestClientUsarbApplication.class, args);
	}
}
