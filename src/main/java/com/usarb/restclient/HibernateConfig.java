package com.usarb.restclient;

import com.usarb.restclient.utils.Constants;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class HibernateConfig {

    @Value("${restclient.usarb.datasource.url.license}")
    private String datasourceUrlLicense;

    @Value("${restclient.usarb.datasource.url.master}")
    private String datasourceUrlMaster;

    @Value("${restclient.usarb.datasource.username}")
    private String datasourceUsername;

    @Value("${restclient.usarb.datasource.password}")
    private String datasourcePassword;

    @Value("${restclient.usarb.datasource.driver}")
    private String datasourceDriver;

    @Value("${restclient.usarb.datasource.dialect}")
    private String datasourceDialect;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(MultiTenantConnectionProvider multiTenantConnectionProvider,
                                                                       CurrentTenantIdentifierResolver currentTenantIdentifierResolver,
                                                                       JpaVendorAdapter jpaVendorAdapter) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(Environment.DIALECT, datasourceDialect);
        properties.put(Environment.NON_CONTEXTUAL_LOB_CREATION, true);
        properties.put(Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
        properties.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProvider);
        properties.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolver);

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setPackagesToScan("com.usarb.restclient");
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setJpaPropertyMap(properties);
        return entityManagerFactoryBean;
    }

    @Bean
    public Map<String, DataSource> dataSourceMap(DataSource dataSourceLicense, DataSource dataSourceMaster) {
        Map<String, DataSource> result = new HashMap<>();
        result.put(Constants.TENANT_MASTER, dataSourceMaster);
        result.put(Constants.TENANT_LICENSE, dataSourceLicense);
        return result;
    }

    @Bean
    public DataSource dataSourceLicense() {
        return DataSourceBuilder.create().driverClassName(datasourceDriver).url(datasourceUrlLicense)
                .username(datasourceUsername).password(datasourcePassword).build();
    }

    @Bean
    public DataSource dataSourceMaster() {
        return DataSourceBuilder.create().driverClassName(datasourceDriver).url(datasourceUrlMaster)
                .username(datasourceUsername).password(datasourcePassword).build();
    }

    @Bean
    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(localContainerEntityManagerFactoryBean.getObject());
        return transactionManager;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }
}
