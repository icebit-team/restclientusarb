package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "d_facultate")
public class Faculty extends TenantEntity {

    @Column(name = "denumire")
    private String name;

    @Column(name = "coduniversitar")
    private Integer universityCode;

    @Column(name = "decan")
    private String manager;

    @Column(name = "engleza")
    private String engName;

    @Column(name = "telefondecan")
    private String phoneManager;

    @Column(name = "telefonmetodist")
    private String phoneMethodist;
}
