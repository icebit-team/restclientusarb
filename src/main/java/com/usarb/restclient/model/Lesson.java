package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "lectii")
public class Lesson {

    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idgrupa")
    private Group group;

    @Column(name = "subgrupa")
    private String subgroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtorent")
    private Torrent torrent;

    @Column(name = "dimensiune")
    private Integer dimension;
}
