package com.usarb.restclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.usarb.restclient.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "auditorii")
public class Classroom extends TenantEntity implements FirebaseEntity {

    @Column(name = "denumire")
    private String name;

    @Column(name = "prescurtare")
    private String shortName;

    @Column(name = "capacitatea")
    private Integer capacity;

    @Column(name = "blocat")
    private Boolean blocked;

    @JsonIgnore
    @OneToMany(mappedBy = "classroom")
    private Set<Timetable> timetables;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Classroom classroom = (Classroom) o;
        return getNameStr().equals(classroom.getNameStr());
    }

    public String getNameStr() {
        return name != null ? name : Strings.EMPTY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNameStr());
    }

    @Override
    public String getTopic() {
        return StringUtils.buildFirebaseTopic(getNameStr());
    }
}
