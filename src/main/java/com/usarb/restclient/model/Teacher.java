package com.usarb.restclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.usarb.restclient.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "profesoriorar")
public class Teacher extends TenantEntity implements FirebaseEntity {

    @Column(name = "nume")
    private String name;

    @Column(name = "numeorar")
    private String nameTimetable;

    @Column(name = "culoare")
    private String color;

    @Column(name = "idcatedra")
    private Long departmentId;

    @Column(name = "idpersoana")
    private Long personId;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "titlu")
    private String title;

    @JsonIgnore
    @OneToMany(mappedBy = "teacher")
    private Set<Torrent> torrents;

    @JsonIgnore
    public String getFullName() {
        return Strings.isNotBlank(title) ? String.format("%s %s", title, name) : name;
    }

    public String getNameStr() {
        return name != null ? name : Strings.EMPTY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return name.equals(teacher.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNameStr());
    }

    @Override
    public String getTopic() {
        return StringUtils.buildFirebaseTopic(getNameStr());
    }
}
