package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "d_catedre")
public class Department extends TenantEntity {

    @Column(name = "idfacultate")
    private Long facultyId;

    @Column(name = "denumire")
    private String name;

    @Column(name = "codcadre")
    private Integer code;
}
