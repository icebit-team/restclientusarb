package com.usarb.restclient.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.usarb.restclient.utils.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "orar")
public class Timetable extends TenantEntity {

    @Column(name = "ziua")
    private String dayName;

    @Column(name = "ora")
    private Integer lessonNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idauditoria")
    private Classroom classroom;

    @Column(name = "saptamina")
    private Integer weekNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idlectie")
    private Lesson lesson;

    @JsonSerialize
    public Integer getDayNumber() {
        return Constants.TIMETABLE_DAYS_NUMBER.get(dayName.trim());
    }

    @JsonSerialize
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public LocalDate getDate() {
        Semester semester = lesson.getTorrent().getSemester();
        LocalDate weekDate = semester.getDate().plusWeeks(this.weekNumber - 1);
        LocalDate monday = weekDate.with(DayOfWeek.MONDAY);
        return monday.plusDays(this.getDayNumber() - 1);
    }

    @JsonIgnore
    public boolean isForSubgroup() {
        return !lesson.getGroup().getName().trim().toLowerCase().equals(lesson.getSubgroup().trim().toLowerCase());
    }
}
