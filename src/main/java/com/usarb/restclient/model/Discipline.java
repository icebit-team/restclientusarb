package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "d_disciplini")
public class Discipline extends TenantEntity {

    @Column(name = "idcatedra")
    private Long departmentId;

    @Column(name = "denumire")
    private String name;

    @Column(name = "engleza")
    private String engName;

    @Column(name = "prescurtare")
    private String shortName;

    @Column(name = "verificat")
    private Boolean checked;
}
