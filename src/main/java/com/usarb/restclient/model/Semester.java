package com.usarb.restclient.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "dataorar")
public class Semester extends TenantEntity {

    @Column(name = "nrsaptamina")
    private Integer weekNumber;

    @Column(name = "semestrul")
    private Integer semesterNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @Column(name = "data")
    private LocalDate date;
}
