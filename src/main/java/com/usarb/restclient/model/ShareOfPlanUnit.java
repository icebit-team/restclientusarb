package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tippondereaunitatilor")
public class ShareOfPlanUnit {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "orientarea")
    private String orientation;

    @Column(name = "nrcredite")
    private Integer creditCounts;
}
