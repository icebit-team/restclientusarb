package com.usarb.restclient.model;

public interface FirebaseEntity {
    String getTopic();
}
