package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "torent")
public class Torrent {

    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idprofesor")
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iddisciplina")
    private Discipline discipline;

    @Column(name = "tipora")
    private String lessonType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "semestrul")
    private Semester semester;

    @Column(name = "nrore")
    private Integer hours;

    @Column(name = "totalore")
    private Integer totalHours;
}
