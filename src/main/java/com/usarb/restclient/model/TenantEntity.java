package com.usarb.restclient.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.usarb.restclient.tenant.TenantContext;
import lombok.Getter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@MappedSuperclass
public class TenantEntity {

    @Transient
    private String database;

    @Id
    @Column(name = "id")
    private Long id;

    @JsonSerialize
    public String getDatabaseHash() {
        return database + id;
    }

    @PostLoad
    public void postLoad() {
        this.database = TenantContext.getTenant();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TenantEntity that = (TenantEntity) o;
        return database.equals(that.database) &&
                id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(database, id);
    }
}
