package com.usarb.restclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.usarb.restclient.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "grupa")
public class Group extends TenantEntity implements FirebaseEntity {

    @Column(name = "idspecialitate")
    private Long specialityId;

    @Column(name = "denumire")
    private String name;

    @Column(name = "cursul")
    private Integer year;

    @Column(name = "active")
    private Boolean active;

    @JsonIgnore
    @OneToMany(mappedBy = "group")
    private Set<Lesson> lessons;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return name.equals(group.name);
    }

    public String getNameStr() {
        return name != null ? name : Strings.EMPTY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNameStr());
    }

    @Override
    public String getTopic() {
        return StringUtils.buildFirebaseTopic(getNameStr());
    }
}
