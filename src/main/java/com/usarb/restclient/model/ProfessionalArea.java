package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "domeniuprofesional")
public class ProfessionalArea {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "cod")
    private String code;

    @Column(name = "denumire")
    private String name;
}
