package com.usarb.restclient.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "d_specialitate")
public class Speciality extends TenantEntity {

    @Column(name = "idfacultate")
    private Long facultyId;

    @Column(name = "idcatedra")
    private Long departmentId;

    @Column(name = "coduniversitar")
    private Integer universityCode;

    @Column(name = "denumire")
    private String name;

    @Column(name = "cod")
    private String code;

    @Column(name = "codvechi")
    private String oldCode;

    @Column(name = "formadeorganizare")
    private String organizationForm;

    @Column(name = "nranistudiu")
    private Integer studyYearsCount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "iddomeniugeneral")
    private GenericArea genericArea;

    @Column(name = "iddomeniugeneral16")
    private Integer genericArea116Id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "iddomeniuprofesional")
    private ProfessionalArea professionalArea;

    @Column(name = "iddomeniuprofesional16")
    private Integer professionalArea116Id;

    @Column(name = "nrcredite")
    private Integer creditNumber;

    @Column(name = "engleza")
    private String engName;

    @Column(name = "titlulrom")
    private String romTitle;

    @Column(name = "titluleng")
    private String engTitle;

    @Column(name = "oredirect")
    private String directHours;

    @Column(name = "oreindividual")
    private String individualHours;

    @Column(name = "statutrom")
    private String romStatus;

    @Column(name = "statuteng")
    private String engStatus;

    @Column(name = "duratadestudiur")
    private String studyDurationR;

    @Column(name = "duratadestudiue")
    private String studyDurationE;

    @Column(name = "idfacultatenoua")
    private Long newFacultyId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idpondereaunitatilorplan")
    private ShareOfPlanUnit shareOfPlanUnits;

    @Column(name = "estemono")
    private Boolean mono;

    @Column(name = "limbadestudiu")
    private String studyLanguage;

    @Column(name = "acreditarerom")
    private String romAccreditation;

    @Column(name = "acreditareeng")
    private String engAccreditation;
}
