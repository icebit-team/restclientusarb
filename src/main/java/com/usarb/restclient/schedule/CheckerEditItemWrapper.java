package com.usarb.restclient.schedule;

import com.usarb.restclient.dto.GenericTimetableDto;
import lombok.Getter;

@Getter
public class CheckerEditItemWrapper {

    private GenericTimetableDto fileValue;

    private GenericTimetableDto dbValue;

    public CheckerEditItemWrapper(GenericTimetableDto fileValue, GenericTimetableDto dbValue) {
        this.fileValue = fileValue;
        this.dbValue = dbValue;
    }
}
