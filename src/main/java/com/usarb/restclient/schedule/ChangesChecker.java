package com.usarb.restclient.schedule;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.usarb.restclient.dto.GenericTimetableDto;
import com.usarb.restclient.model.Semester;
import com.usarb.restclient.model.Timetable;
import com.usarb.restclient.service.FirebaseService;
import com.usarb.restclient.service.SemesterService;
import com.usarb.restclient.service.TimetableService;
import com.usarb.restclient.utils.TimetableFilter;
import com.usarb.restclient.utils.YearInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
public class ChangesChecker {

    private static final String FOLDER_PATH = "timetable_changes_check_store";
    private static final String GROUP_ITEMS_FILE_NAME = "group_items_state.json";
    private static final String TEACHER_ITEMS_FILE_NAME = "teacher_items_state.json";
    private static final String CLASSROOM_ITEMS_FILE_NAME = "classroom_items_state.json";
    private static final String INFO_FILE_NAME = "info_state.json";

    @Resource
    private TimetableService timetableService;

    @Resource
    private SemesterService semesterService;

    @Resource
    private FirebaseService firebaseService;

    private ObjectMapper objectMapper;
    private MapType mapType;

    @PostConstruct
    public void post() {
        this.objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, GenericTimetableDto.class);
        mapType = objectMapper.getTypeFactory().constructMapType(HashMap.class, objectMapper.getTypeFactory().constructType(String.class), collectionType);

    }

    @Scheduled(fixedDelay = 1000 * 60 * 15)
    public void schedule() throws IOException {
        CheckerInfo currentInfo = null;

        try {
            log.info("---------------------------------- START ----------------------------------");

            FilesInfo filesInfo = readFiles();
            currentInfo = buildInfo();

            log.info("current info: semester#{}, week#{}, date#{}, ", currentInfo.getSemester().getSemesterNumber(),
                    currentInfo.getWeek(), currentInfo.getDate().format(DateTimeFormatter.ISO_DATE));

            //if is first check or year is changed
            if (filesInfo.isEmpty() || !currentInfo.getYearInfo().equals(filesInfo.getInfo().getYearInfo())) {
                log.info("missed check, files are empty or year is changed");
                writeFiles(currentInfo);
            } else {
                log.info("comparing");

                List<Timetable> timetables = loadItems(filesInfo.getInfo());
                Map<String, List<GenericTimetableDto>> groupItems = GenericTimetableDto.instanceForGroups(timetables);
                Map<String, List<GenericTimetableDto>> teacherItems = GenericTimetableDto.instanceForTeachers(timetables);
                Map<String, List<GenericTimetableDto>> classroomItems = GenericTimetableDto.instanceForClassrooms(timetables);

                log.info("check items");
                log.info("-for groups");
                ChangesInfo groupChanges = checkChanges(groupItems, filesInfo.getGroupItems());
                log.info("-for teachers");
                ChangesInfo teacherChanges = checkChanges(teacherItems, filesInfo.getTeacherItems());
                log.info("-for classrooms");
                ChangesInfo classroomChanges = checkChanges(classroomItems, filesInfo.getClassroomItems());

                firebaseService.notify(groupChanges, true, false, false);
                firebaseService.notify(teacherChanges, false, true, false);
                firebaseService.notify(classroomChanges, false, false, true);
            }
        } catch (Exception e) {
            log.error("ChangesChecker error", e);
        } finally {
            if (currentInfo != null) {
                writeFiles(currentInfo);
            }

            log.info("---------------------------------- STOP -----------------------------------");
        }
    }

    private ChangesInfo checkChanges(Map<String, List<GenericTimetableDto>> dbValues, Map<String, List<GenericTimetableDto>> fileValues) {
        log.info("-- file {}, database {}", fileValues.size(), dbValues.size());

        ChangesInfo changesInfo = new ChangesInfo();
        for (Map.Entry<String, List<GenericTimetableDto>> dbEntry : dbValues.entrySet()) {
            List<GenericTimetableDto> fileEntryValue = fileValues.getOrDefault(dbEntry.getKey(), new ArrayList<>());

            List<GenericTimetableDto> newItems = dbEntry.getValue().stream().filter(dbDto -> !fileEntryValue.contains(dbDto)).collect(toList());
            if (!newItems.isEmpty()) {
                List<GenericTimetableDto> newItemsStore = changesInfo.getNewItems().computeIfAbsent(dbEntry.getKey(), k -> new ArrayList<>());
                newItemsStore.addAll(newItems);
            }

            List<GenericTimetableDto> deleteItems = fileEntryValue.stream().filter(fileDto -> !dbEntry.getValue().contains(fileDto)).collect(toList());
            if (!deleteItems.isEmpty()) {
                List<GenericTimetableDto> deleteItemsStore = changesInfo.getDeleteItems().computeIfAbsent(dbEntry.getKey(), k -> new ArrayList<>());
                deleteItemsStore.addAll(deleteItems);
            }

            List<GenericTimetableDto> dbItems = dbEntry.getValue().stream().filter(fileEntryValue::contains).collect(toList());
            Map<Integer, GenericTimetableDto> fileItems = fileEntryValue.stream().filter(fileDto -> dbEntry.getValue().contains(fileDto)).collect(groupingBy(Object::hashCode))
                    .entrySet().stream().collect(toMap(Map.Entry::getKey, o -> o.getValue().get(0)));

            if (dbItems.size() != fileItems.size()) {
                throw new RuntimeException("Something went wrong!");
            }

            List<CheckerEditItemWrapper> editItems = dbItems.stream().filter(dto -> !fileItems.get(dto.hashCode()).getItems().containsAll(dto.getItems()) || !dto.getItems().containsAll(fileItems.get(dto.hashCode()).getItems()))
                    .map(dto -> new CheckerEditItemWrapper(dto, fileItems.get(dto.hashCode()))).collect(toList());
            if (!editItems.isEmpty()) {
                List<CheckerEditItemWrapper> editItemsStore = changesInfo.getEditItems().computeIfAbsent(dbEntry.getKey(), k -> new ArrayList<>());
                editItemsStore.addAll(editItems);
            }
        }

        if (changesInfo.hasChanges()) {
            if (!changesInfo.getNewItems().isEmpty()) {
                log.info("---- new #{}", changesInfo.getNewItems().size());
            }
            if (!changesInfo.getDeleteItems().isEmpty()) {
                log.info("---- removed #{}", changesInfo.getDeleteItems().size());
            }
            if (!changesInfo.getEditItems().isEmpty()) {
                log.info("---- edited #{}", changesInfo.getEditItems().size());
            }
        } else {
            log.info("-- not found changes");
        }

        return changesInfo;
    }

    private CheckerInfo buildInfo() {
        LocalDate currentDate = LocalDate.now();
        YearInfo yearInfo = semesterService.determineYear(null);
        Semester semester = semesterService.determineSemesterByWeekDate(yearInfo, currentDate);
        Integer week = semesterService.determineWeekNumber(semester, currentDate);
        return new CheckerInfo(yearInfo, semester, week, currentDate);
    }

    private void writeFiles(CheckerInfo checkerInfo) throws IOException {
        log.info("write files for: semester#{}, week#{}, date#{}, ", checkerInfo.getSemester().getSemesterNumber(),
                checkerInfo.getWeek(), checkerInfo.getDate().format(DateTimeFormatter.ISO_DATE));

        objectMapper.writeValue(getFileToWrite(INFO_FILE_NAME), checkerInfo);

        List<Timetable> timetables = loadItems(checkerInfo);
        objectMapper.writeValue(getFileToWrite(GROUP_ITEMS_FILE_NAME), GenericTimetableDto.instanceForGroups(timetables));
        objectMapper.writeValue(getFileToWrite(TEACHER_ITEMS_FILE_NAME), GenericTimetableDto.instanceForTeachers(timetables));
        objectMapper.writeValue(getFileToWrite(CLASSROOM_ITEMS_FILE_NAME), GenericTimetableDto.instanceForClassrooms(timetables));

        log.info("-- files was saved");
    }

    private FilesInfo readFiles() throws IOException {
        log.info("read files");

        Paths.get(FOLDER_PATH).toFile().mkdir();

        FilesInfo filesInfo = new FilesInfo();

        File infoFile = Paths.get(FOLDER_PATH, INFO_FILE_NAME).toFile();
        if (infoFile.exists()) {
            filesInfo.setInfo(objectMapper.readValue(infoFile, CheckerInfo.class));
        }

        filesInfo.setGroupItems(readFile(GROUP_ITEMS_FILE_NAME));
        filesInfo.setTeacherItems(readFile(TEACHER_ITEMS_FILE_NAME));
        filesInfo.setClassroomItems(readFile(CLASSROOM_ITEMS_FILE_NAME));

        if (filesInfo.getInfo() == null) {
            log.info("-- files was not found");
        } else {
            log.info("-- files info: semester#{}, week#{}, date#{}, ", filesInfo.getInfo().getSemester().getSemesterNumber(),
                    filesInfo.getInfo().getWeek(), filesInfo.getInfo().getDate().format(DateTimeFormatter.ISO_DATE));
            log.info("---- items {}", filesInfo.getGroupItems().size() + filesInfo.getTeacherItems().size() + filesInfo.getClassroomItems().size());
        }

        return filesInfo;
    }

    private Map<String, List<GenericTimetableDto>> readFile(String fileName) throws IOException {
        File groupFile = Paths.get(FOLDER_PATH, fileName).toFile();
        try {
            if (groupFile.exists()) {
                return objectMapper.readValue(groupFile, mapType);
            } else {
                return new HashMap<>();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new HashMap<>();
        }
    }

    private List<Timetable> loadItems(CheckerInfo checkerInfo) {
        log.info("query for: semester#{}, week#{}, date#{}", checkerInfo.getSemester().getSemesterNumber(),
                checkerInfo.getWeek(), checkerInfo.getDate().format(DateTimeFormatter.ISO_DATE));
        List<Timetable> result = timetableService.findCriteriaAll(TimetableFilter.build(checkerInfo));
        log.info("-- result size#{}", result.size());
        return result;
    }

    private File getFileToWrite(String fileName) throws IOException {
        Path path = Paths.get(FOLDER_PATH, fileName);

        if (Files.exists(path)) {
            Files.delete(path);
        } else {
            Files.createFile(path);
        }

        return path.toFile();
    }
}
