package com.usarb.restclient.schedule;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usarb.restclient.model.Semester;
import com.usarb.restclient.utils.YearInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckerInfo {

    private YearInfo yearInfo;

    private Semester semester;

    private Integer week;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate date;
}
