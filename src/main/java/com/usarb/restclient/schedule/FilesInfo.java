package com.usarb.restclient.schedule;

import com.usarb.restclient.dto.GenericTimetableDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Setter
@Getter
public class FilesInfo {

    private CheckerInfo info;

    private Map<String, List<GenericTimetableDto>> groupItems;

    private Map<String, List<GenericTimetableDto>> teacherItems;

    private Map<String, List<GenericTimetableDto>> classroomItems;

    public boolean isEmpty() {
        return info == null;
    }
}
