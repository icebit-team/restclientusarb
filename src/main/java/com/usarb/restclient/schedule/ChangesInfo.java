package com.usarb.restclient.schedule;

import com.usarb.restclient.dto.GenericTimetableDto;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ChangesInfo {

    private Map<String, List<GenericTimetableDto>> newItems = new HashMap<>();

    private Map<String, List<CheckerEditItemWrapper>> editItems = new HashMap<>();

    private Map<String, List<GenericTimetableDto>> deleteItems = new HashMap<>();

    public boolean hasChanges() {
        return !newItems.isEmpty() || !editItems.isEmpty() || !deleteItems.isEmpty();
    }
}
