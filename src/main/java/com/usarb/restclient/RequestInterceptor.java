package com.usarb.restclient;

import com.usarb.restclient.utils.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RequestInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod && !request.getDispatcherType().equals(DispatcherType.ERROR)) {
            List<String> currentParams = new ArrayList<>(request.getParameterMap().keySet());
            List<String> expectedParams = Arrays.stream(((HandlerMethod) handler).getMethodParameters()).map(methodParameter -> methodParameter.getParameter().getName()).collect(Collectors.toList());
            List<String> currentNotExpected = currentParams.stream().filter(s -> !expectedParams.contains(s)).collect(Collectors.toList());

            if (!currentNotExpected.isEmpty()) {
                String msg = "Undefined params: " + String.join(", ", currentNotExpected) + ". Available: " + String.join(", ", expectedParams);
                response.getWriter().write(msg);
                throw new ApplicationException(msg);
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("REQUEST '{}' PARAMS '{}'", request.getRequestURI(), request.getParameterMap().entrySet().stream()
                .map(entry -> String.format("%s=%s", entry.getKey(), String.join(",", entry.getValue())))
                .collect(Collectors.joining("&")));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
    }
}
