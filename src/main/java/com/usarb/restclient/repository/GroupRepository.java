package com.usarb.restclient.repository;

import com.usarb.restclient.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {

    List<Group> findAllByYearOrderByName(Integer year);

    List<Group> findAllByNameContainingOrderByName(String name);

    List<Group> findAllByYearAndNameContainingOrderByName(Integer year, String name);
}
