package com.usarb.restclient.repository;

import com.usarb.restclient.model.Semester;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SemesterRepository extends JpaRepository<Semester, Long> {

    Semester findBySemesterNumber(Integer semesterNumber);
}
