package com.usarb.restclient.repository;

import com.usarb.restclient.model.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {

    List<Classroom> findAllByNameContainingOrderByName(String name);
}
