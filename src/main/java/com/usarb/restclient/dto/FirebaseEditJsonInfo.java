package com.usarb.restclient.dto;

import com.usarb.restclient.schedule.CheckerEditItemWrapper;
import lombok.Getter;
import lombok.Setter;

import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class FirebaseEditJsonInfo {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private Integer nr;
    private String date;

    private String oldType;
    private String oldDisc;
    private String oldPInfo;
    private String oldSInfo;

    private String newType;
    private String newDisc;
    private String newPInfo;
    private String newSInfo;

    public FirebaseEditJsonInfo(CheckerEditItemWrapper dto) {
        this.nr = dto.getDbValue().getLessonNumber();
        this.date = dto.getDbValue().getDate().format(DATE_FORMATTER);

        Set<GenericTimetableItemDto> dbItems = dto.getDbValue().getItems();
        Set<GenericTimetableItemDto> fileItems = dto.getFileValue().getItems();

        if (dto.getDbValue().getItems().size() > 1 || dto.getFileValue().getItems().size() > 1) {
            this.oldType = dbItems.stream().map(GenericTimetableItemDto::getLessonType).map(s -> s.substring(0, 3)).collect(Collectors.joining("* "));
            this.oldDisc = dbItems.stream().map(GenericTimetableItemDto::getDiscipline).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));
            this.oldPInfo = dbItems.stream().map(GenericTimetableItemDto::getPrimaryInfo).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));
            this.oldSInfo = dbItems.stream().map(GenericTimetableItemDto::getSecondaryInfo).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));

            this.newType = fileItems.stream().map(GenericTimetableItemDto::getLessonType).map(s -> s.substring(0, 3)).collect(Collectors.joining("* "));
            this.newDisc = fileItems.stream().map(GenericTimetableItemDto::getDiscipline).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));
            this.newPInfo = fileItems.stream().map(GenericTimetableItemDto::getPrimaryInfo).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));
            this.newSInfo = fileItems.stream().map(GenericTimetableItemDto::getSecondaryInfo).map(s -> s.length() > 6 ? s.substring(0, 6) : s).collect(Collectors.joining("* "));
        } else {
            this.oldType = dbItems.stream().map(GenericTimetableItemDto::getLessonType).collect(Collectors.joining());
            this.oldDisc = dbItems.stream().map(GenericTimetableItemDto::getDiscipline).collect(Collectors.joining());
            this.oldPInfo = dbItems.stream().map(GenericTimetableItemDto::getPrimaryInfo).collect(Collectors.joining());
            this.oldSInfo = dbItems.stream().map(GenericTimetableItemDto::getSecondaryInfo).collect(Collectors.joining());

            this.newType = fileItems.stream().map(GenericTimetableItemDto::getLessonType).collect(Collectors.joining());
            this.newDisc = fileItems.stream().map(GenericTimetableItemDto::getDiscipline).collect(Collectors.joining());
            this.newPInfo = fileItems.stream().map(GenericTimetableItemDto::getPrimaryInfo).collect(Collectors.joining());
            this.newSInfo = fileItems.stream().map(GenericTimetableItemDto::getSecondaryInfo).collect(Collectors.joining());
        }
    }
}
