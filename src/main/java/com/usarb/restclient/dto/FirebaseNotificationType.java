package com.usarb.restclient.dto;

public enum FirebaseNotificationType {
    EDIT, DELETE, NEW;
}
