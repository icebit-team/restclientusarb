package com.usarb.restclient.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usarb.restclient.model.*;
import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import java.time.LocalDate;

@Getter
public class TimetableDto {

    private String database;

    private Long id;

    private Integer lessonNumber;

    private String dayName;

    private Integer dayNumber;

    private Integer semesterNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate date;

    private Integer weekNumber;

    private Long classroomId;

    private String classroomName;

    private Long groupId;

    private String groupName;

    private String subgroupName;

    private Boolean subgroup;

    private Long teacherId;

    private String teacherName;

    private String teacherShortName;

    private String teacherLongName;

    private Long disciplineId;

    private String disciplineName;

    private String lessonType;

    public TimetableDto(Timetable timetable) {
        this.database = timetable.getDatabase();
        this.id = timetable.getId();
        this.lessonNumber = timetable.getLessonNumber();
        this.dayName = timetable.getDayName().trim();
        this.dayNumber = timetable.getDayNumber();
        this.weekNumber = timetable.getWeekNumber();
        this.date = timetable.getDate();

        Classroom classroom = timetable.getClassroom();

        if (classroom != null) {
            this.classroomId = classroom.getId();
            this.classroomName = classroom.getNameStr();
        }

        Lesson lesson = timetable.getLesson();
        Group group = lesson.getGroup();

        this.groupId = group.getId();
        this.groupName = group.getName();
        this.subgroup = timetable.isForSubgroup();
        this.subgroupName = lesson.getSubgroup();

        Torrent torrent = lesson.getTorrent();
        Teacher teacher = torrent.getTeacher();

        this.teacherId = teacher.getId();
        this.teacherName = teacher.getName();
        this.teacherShortName = teacher.getNameTimetable();
        this.teacherLongName = teacher.getFullName();

        Discipline discipline = torrent.getDiscipline();

        this.disciplineId = discipline.getId();
        this.disciplineName = discipline.getName();
        this.lessonType = torrent.getLessonType();

        this.semesterNumber = torrent.getSemester().getSemesterNumber();
    }
}
