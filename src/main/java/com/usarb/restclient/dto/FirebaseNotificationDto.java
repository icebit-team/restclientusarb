package com.usarb.restclient.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class FirebaseNotificationDto {

    private String title;

    private String body;

    private String sound = "default";

    public FirebaseNotificationDto(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
