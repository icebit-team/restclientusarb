package com.usarb.restclient.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class FirebaseNewOrDeleteJsonInfo {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private Integer nr;
    private String disc;
    private String date;

    public FirebaseNewOrDeleteJsonInfo(GenericTimetableDto dto) {
        this.nr = dto.getLessonNumber();

        List<String> disciplines = dto.getItems().stream().map(GenericTimetableItemDto::getShortInfo).collect(Collectors.toList());
        if (disciplines.size() == 1) {
            this.disc = disciplines.get(0);
        } else {
            this.disc = String.format("%s | %s", disciplines.get(0).substring(0, 10), disciplines.get(1).substring(0, 10));
        }

        this.date = dto.getDate().format(DATE_FORMATTER);
    }
}
