package com.usarb.restclient.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.usarb.restclient.schedule.CheckerEditItemWrapper;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
public class FirebaseDataDto {

    private String topic;

    private String topicInfo;

    private String date;

    private boolean group;

    private boolean teacher;

    private boolean classroom;

    private FirebaseNotificationType type;

    private String jsonInfo;

    public FirebaseDataDto(String topic, String topicInfo, boolean group, boolean teacher, boolean classroom, FirebaseNotificationType type) {
        this.topic = topic;
        this.topicInfo = topicInfo;

        this.group = group;
        this.teacher = teacher;
        this.classroom = classroom;
        this.type = type;
    }

    public FirebaseDataDto(String topic, List<GenericTimetableDto> dtos, boolean group, boolean teacher, boolean classroom, FirebaseNotificationType type, String topicInfo) throws JsonProcessingException {
        this(topic, topicInfo, group, teacher, classroom, type);

        ObjectMapper objectMapper = new ObjectMapper();
        List<FirebaseNewOrDeleteJsonInfo> result = dtos.stream().map(FirebaseNewOrDeleteJsonInfo::new).collect(Collectors.toList());
        this.jsonInfo = objectMapper.writeValueAsString(result);
    }

    public FirebaseDataDto(String topic, CheckerEditItemWrapper dto, boolean group, boolean teacher, boolean classroom, String topicInfo) throws JsonProcessingException {
        this(topic, topicInfo, group, teacher, classroom, FirebaseNotificationType.EDIT);

        ObjectMapper objectMapper = new ObjectMapper();
        this.jsonInfo = objectMapper.writeValueAsString(new FirebaseEditJsonInfo(dto));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FirebaseDataDto that = (FirebaseDataDto) o;
        return group == that.group &&
                teacher == that.teacher &&
                classroom == that.classroom &&
                Objects.equals(topic, that.topic) &&
                Objects.equals(topicInfo, that.topicInfo) &&
                Objects.equals(date, that.date) &&
                type == that.type &&
                Objects.equals(jsonInfo, that.jsonInfo);
    }

    @JsonSerialize
    @Override
    public int hashCode() {
        return Objects.hash(topic, topicInfo, date, group, teacher, classroom, type, jsonInfo);
    }
}
