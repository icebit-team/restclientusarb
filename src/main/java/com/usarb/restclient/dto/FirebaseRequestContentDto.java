package com.usarb.restclient.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.usarb.restclient.schedule.CheckerEditItemWrapper;
import com.usarb.restclient.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class FirebaseRequestContentDto {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final String TOPIC_TPL = "'%s' in topics || 'ANY_TOPIC' in topics";

    private String condition;

    private String priority = "high";

    private FirebaseNotificationDto notification;

    private FirebaseDataDto data;

    public FirebaseRequestContentDto(String topicInfo, List<GenericTimetableDto> dtos, boolean group, boolean teacher, boolean classroom, FirebaseNotificationType type) throws JsonProcessingException {
        String topic = StringUtils.buildFirebaseTopic(topicInfo);
        this.condition = String.format(TOPIC_TPL, topic);

        String title;
        String body;
        if (type.equals(FirebaseNotificationType.DELETE)) {
            title = "DELETE !!!";
        } else {
            title = "NEW !!!";
        }

        Set<LocalDate> dates = dtos.stream().map(GenericTimetableDto::getDate).collect(Collectors.toSet());
        if (dates.size() == 1) {
            body = String.format("%s | %s", topicInfo, dates.iterator().next().format(DATE_FORMATTER));
        } else {
            body = String.format("%s | %s - %s", topicInfo,
                    dates.stream().min(LocalDate::compareTo).orElse(LocalDate.now()).format(DATE_FORMATTER),
                    dates.stream().max(LocalDate::compareTo).orElse(LocalDate.now()).format(DATE_FORMATTER));
        }

        this.notification = new FirebaseNotificationDto(title, body);
        this.data = new FirebaseDataDto(topic, dtos, group, teacher, classroom, type, topicInfo);
    }

    public FirebaseRequestContentDto(String topicInfo, CheckerEditItemWrapper dto, boolean group, boolean teacher, boolean classroom) throws JsonProcessingException {
        String topic = StringUtils.buildFirebaseTopic(topicInfo);
        this.condition = String.format(TOPIC_TPL, topic);

        String title = "EDIT !!!";
        String body = String.format("%s | %s", topicInfo, dto.getDbValue().getDate().format(DATE_FORMATTER));
        this.notification = new FirebaseNotificationDto(title, body);
        this.data = new FirebaseDataDto(topic, dto, group, teacher, classroom, topicInfo);
    }
}
