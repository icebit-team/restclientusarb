package com.usarb.restclient.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.usarb.restclient.model.Timetable;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.usarb.restclient.utils.TimetableBuilder.buildGenericTimetableResponse;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"date", "lessonNumber"})
public class GenericTimetableDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate date;

    private Integer lessonNumber;

    private Set<GenericTimetableItemDto> items;

    private GenericTimetableDto(Timetable timetable) {
        this.lessonNumber = timetable.getLessonNumber();
        this.date = timetable.getDate();
    }

    public static Map<String, List<GenericTimetableDto>> instanceForTeachers(List<Timetable> value) {
        try {
            return value.stream().collect(Collectors.groupingBy(timetable -> timetable.getLesson().getTorrent().getTeacher(), Collectors.toList())).entrySet().stream()
                    .filter(teacherListEntry -> !Strings.isBlank(teacherListEntry.getKey().getName()))
                    .collect(Collectors.toMap(entry -> entry.getKey().getName(), entry -> buildGenericTimetableResponse(entry.getValue(), lessonEntry -> GenericTimetableDto.instanceForTeacher(lessonEntry.getValue()))));
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new HashMap<>();
        }
    }

    public static GenericTimetableDto instanceForTeacher(List<Timetable> value) {
        Timetable timetable = value.get(0);
        GenericTimetableDto genericInstance = new GenericTimetableDto(timetable);
        genericInstance.setItems(Collections.singleton(new GenericTimetableItemDto(timetable, timetable.getClassroom(), value)));
        return genericInstance;
    }

    public static Map<String, List<GenericTimetableDto>> instanceForGroups(List<Timetable> value) {
        try {
            return value.stream().collect(Collectors.groupingBy(timetable -> timetable.getLesson().getGroup(), Collectors.toList())).entrySet().stream()
                    .filter(groupListEntry -> !Strings.isBlank(groupListEntry.getKey().getName()))
                    .collect(Collectors.toMap(entry -> entry.getKey().getName(), entry -> buildGenericTimetableResponse(entry.getValue(), lessonEntry -> GenericTimetableDto.instanceForGroup(lessonEntry.getValue()))));
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new HashMap<>();
        }
    }

    public static GenericTimetableDto instanceForGroup(List<Timetable> value) {
        GenericTimetableDto genericInstance = new GenericTimetableDto(value.get(0));
        Set<GenericTimetableItemDto> items = value.stream()
                .sorted(Comparator.comparing(timetable -> timetable.getLesson().getSubgroup()))
                .map(timetable -> new GenericTimetableItemDto(timetable, timetable.getClassroom()))
                .collect(Collectors.toSet());
        genericInstance.setItems(items);
        return genericInstance;
    }

    public static Map<String, List<GenericTimetableDto>> instanceForClassrooms(List<Timetable> value) {
        try {
            return value.stream()
                    .filter(timetable -> timetable.getClassroom() != null)
                    .collect(Collectors.groupingBy(Timetable::getClassroom, Collectors.toList())).entrySet().stream()
                    .filter(classroomListEntry -> !Strings.isBlank(classroomListEntry.getKey().getName()))
                    .collect(Collectors.toMap(entry -> entry.getKey().getNameStr(), entry -> buildGenericTimetableResponse(entry.getValue(), lessonEntry -> GenericTimetableDto.instanceForClassroom(lessonEntry.getValue()))));
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new HashMap<>();
        }
    }

    public static GenericTimetableDto instanceForClassroom(List<Timetable> value) {
        Timetable timetable = value.get(0);
        GenericTimetableDto genericInstance = new GenericTimetableDto(timetable);
        genericInstance.setItems(Collections.singleton(new GenericTimetableItemDto(timetable, timetable.getLesson().getTorrent().getTeacher(), value)));
        return genericInstance;
    }
}
