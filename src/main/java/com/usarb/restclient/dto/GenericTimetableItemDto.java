package com.usarb.restclient.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.usarb.restclient.model.Classroom;
import com.usarb.restclient.model.Lesson;
import com.usarb.restclient.model.Teacher;
import com.usarb.restclient.model.Timetable;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class GenericTimetableItemDto {

    private String lessonType;

    private String discipline;

    private String primaryInfo;

    private String secondaryInfo;

    @JsonIgnore
    public String getShortInfo() {
        return String.format("%s* %s*", lessonType.substring(0, 5), discipline.length() > 20 ? discipline.substring(0, 20) : discipline);
    }

    private GenericTimetableItemDto(Timetable timetable) {
        this.lessonType = timetable.getLesson().getTorrent().getLessonType().toUpperCase();
        this.discipline = timetable.getLesson().getTorrent().getDiscipline().getName();
    }

    GenericTimetableItemDto(Timetable timetable, Classroom classroom) {
        String lessonType = timetable.getLesson().getTorrent().getLessonType().toUpperCase();
        this.lessonType = timetable.isForSubgroup() ? String.format("%s [%s]", lessonType, timetable.getLesson().getSubgroup()) : lessonType;
        this.discipline = timetable.getLesson().getTorrent().getDiscipline().getName();
        this.secondaryInfo = classroom != null ? classroom.getNameStr() : "N/A";
        this.primaryInfo = timetable.getLesson().getTorrent().getTeacher().getFullName();
    }

    GenericTimetableItemDto(Timetable timetable, Teacher teacher, List<Timetable> value) {
        this(timetable);
        this.secondaryInfo = buildGroupPrimaryInfo(value);
        this.primaryInfo = teacher.getFullName();
    }

    GenericTimetableItemDto(Timetable timetable, Classroom classroom, List<Timetable> value) {
        this(timetable);
        this.secondaryInfo = classroom != null ? classroom.getNameStr() : "N/A";
        this.primaryInfo = buildGroupPrimaryInfo(value);
    }

    /**
     * for TEACHER/CLASSROOM builder
     * Collects all Group names with preventive subgroup check
     */
    private static String buildGroupPrimaryInfo(List<Timetable> value) {
        return value.stream().map(item -> {
            Lesson lesson = item.getLesson();
            return item.isForSubgroup() ? String.format("[%s] %s", lesson.getSubgroup(), lesson.getGroup().getName()) : lesson.getGroup().getName();
        }).sorted().distinct().collect(Collectors.joining(", "));
    }
}
