package com.usarb.restclient;

import com.usarb.restclient.utils.ApplicationException;
import com.usarb.restclient.utils.UnsupportedDateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<String> springHandleNotFound(Exception ex) {
        String msg;

        if (ex.getCause() instanceof ApplicationException) {
            msg = ex.getCause().getMessage();
        } else {
            msg = ex.getMessage();
        }

        log.warn(msg);
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(msg);
    }

    @ExceptionHandler(UnsupportedDateException.class)
    public ResponseEntity<List<Object>> unsupportedDateException(Exception ex) {
        log.warn(ex.getMessage());
        return ResponseEntity.ok(new ArrayList<>());
    }
}
